..
 # SPDX-FileCopyrightText: Copyright (c) 2023, Linaro Limited.
 #
 # SPDX-FileCopyrightText: <text>Copyright 2022-2024 Arm Limited and/or its
 # affiliates <open-source-office@arm.com></text>
 #
 # SPDX-License-Identifier: MIT

############
Introduction
############

Project Cassini is the open, collaborative, standards-based initiative to
deliver a seamless cloud-native software experience for devices based on Arm
Cortex-A.

Current release of Cassini distribution provides a framework for deployment and
orchestration of applications (edge runtime) within containers and support for
platform abstraction for security (PARSEC).

Future releases of Cassini distribution will include support for provisioning
the platform and update all components of software stack over the air. In
addition, optionally utilize PARSEC to secure those operations.

******************
Use-Case Overview
******************

Cassini aims to facilitate the deployment of application workloads via Docker
and K3s use-case on the supported target platforms.

Instructions for achieving these use-cases are given in the
:ref:`build <user_manual/build:Build, Deploy and Validate Cassini Image>`
section, subject to relevant assumed technical knowledge as listed later in
:ref:`documentation assumptions <documentation_assumptions_label>`.

************
Architecture
************

The following diagram illustrates the Cassini Architecture.

.. image:: images/cassini_architecture.png
   :align: center

The different software layers are described below:

  * **Application workloads**:

    User-defined container applications that are deployed and executed on the
    Cassini software stack. Note that the Cassini project provides the system
    infrastructure for user workloads, and not the application workloads
    themselves. Instead, they should be deployed by end-users according to their
    individual use-cases.

  * **Linux-based filesystem**:

    This is the main component provided by the Cassini project. The Cassini
    filesystem contains tools and services that provide Cassini core
    functionalities and facilitate secure deployment and orchestration of user
    application workloads. These tools and services include the Parsec service,
    the Docker container engine, the K3s container orchestration framework,
    together with their run-time dependencies. In addition, Cassini provides
    supporting packages such as those which enable run-time validation tests or
    software development capabilities on the target platform.

  * **System software**:

    System software specific to the target platform, composed of firmware,
    bootloader and the operating system.

*****************
Features Overview
*****************

Cassini includes the following major features:

  * Container engine and runtime with Docker and runc-opencontainers.
  * Container workload orchestration with the K3s Kubernetes distribution.
  * Parsec service and Parsec tool
  * On-target development support with optionally included Software Development
    Kit.
  * Validation support with optionally included run-time integration tests, and
    build-time kernel configuration checks.

Other features of Cassini include:

  * The features provided by the ``poky.conf`` distribution, which Cassini
    extends.
  * Systemd used as the init system.
  * RPM used as the package management system.

.. _documentation_assumptions_label:

Documentation Assumptions
=========================

This documentation assumes a base level of knowledge related to different
aspects of achieving the target use-case via Cassini:

  * Application workload containerization, deployment, and orchestration

    This documentation does not provide detailed guidance on developing
    application workloads, deploying them, or managing their execution via
    Docker or the K3s orchestration framework, and instead focuses on
    Cassini-specific instructions to support these activities on an Cassini
    distribution image.

    For information on how to use these technologies which are provided with the
    Cassini distribution, see the |Docker documentation|_ and the
    |K3s documentation|_.

  * The Yocto Project

    This documentation contains instructions for achieving Cassini's use-case
    using a set of included configuration files that provide standard build
    features and settings. However, Cassini forms a distribution layer for
    integration with the Yocto project and is thus highly configurable and
    extensible. This documentation supports those activities by detailing the
    available options for Cassini-specific customizations and extensions, but
    assumes knowledge of the Yocto project necessary to prepare an appropriate
    build environment with these options configured.

    Readers are referred to the |Yocto Project Documentation|_ for information
    on setting up and running non-standard Cassini distribution builds.

********************
Repository Structure
********************

The ``meta-cassini`` repository is structured as follows:

  * ``meta-cassini``:

    * ``meta-cassini-bsp``

      A Yocto layer which holds board-specific recipes or append files that
      either:

      * will not be upstreamed (Cassini specific modifications)

      * have not been upstreamed yet

    * ``meta-cassini-distro``

      A Yocto distribution layer providing top-level and general policies for
      the Cassini distribution images.

    * ``meta-cassini-tests``

      A Yocto software layer with recipes that include run-time tests to
      validate Cassini functionalities.

    * ``kas``

      Directory which contains files to support use of the kas build tool.

******************
Repository License
******************

The repository's standard license is the MIT license (more details in
:ref:`license_link:License`), under which most of the repository's content is
provided. Exceptions to this standard license relate to files that represent
modifications to externally licensed works (for example, patch files). These
files may therefore be included in the repository under alternative licenses in
order to be compliant with the licensing requirements of the associated
external works.

Contributions to the project should follow the same licensing arrangement.

*********************************
Contributions and Issue Reporting
*********************************

Guidance for contributing to the Cassini project can be found at
:doc:`contributing`.

To report issues with the repository such as potential bugs, security concerns,
or feature requests, please submit an Issue via |GitLab Issues|_, following the
project's template.

For known issues in this release, see :doc:`Release Notes </release_notes/v1.0.1>`.

********************
Feedback and support
********************

To request support please contact Linaro at support@linaro.org.

.. _maintainers_label:

*************
Maintainer(s)
*************

- Cassini Team
