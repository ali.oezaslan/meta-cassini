..
 # SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
 # affiliates <open-source-office@arm.com></text>
 #
 # SPDX-License-Identifier: MIT

############
Yocto Layers
############

The ``meta-cassini`` repository provides three layers compatible with the Yocto
Project, in the following sub-directories:

  * ``meta-cassini-bsp``

    A Yocto layer which holds board-specific recipes or append files that
    either:

      * will not be upstreamed (Cassini specific modifications)

      * have not been upstreamed yet

      * For the N1SDP hardware target platform, this layer currently extends
        the n1sdp machine definition from the meta-arm-bsp layer with additional
        Trusted Services (crypto, storage, internal trusted storage, attestation
        , block storage)

  * ``meta-cassini-distro``

    A Yocto distribution layer providing top-level and general policies for the
    Cassini distribution images.

  * ``meta-cassini-tests``

    A Yocto software layer with recipes that include run-time tests to validate
    Cassini functionalities.

.. _layer_dependency_overview_label:

*************************
Layer Dependency Overview
*************************

The following diagram illustrates the layers which are integrated by the
Cassini project, which are further expanded on below.
|layer dependency statement|

|

.. image:: ../images/cassini_layers_deps.png
   :align: center

|

Cassini distribution depends on the following layer dependency sources:

  .. code-block:: yaml
    :substitutions:

    URL: https://git.yoctoproject.org/git/poky
    layers: meta, meta-poky
    branch: |poky branch|
    revision: |poky revision|

    URL: https://git.openembedded.org/meta-openembedded
    layers: meta-filesystems, meta-networking, meta-oe, meta-python
    branch: |meta-openembedded branch|
    revision: |meta-openembedded revision|

    URL: https://git.yoctoproject.org/git/meta-virtualization
    layer: meta-virtualization
    branch: |meta-virtualization branch|
    revision: |meta-virtualization revision|

    URL: https://git.yoctoproject.org/git/meta-security
    layers: meta-parsec
    branch: |meta-security branch|
    revision: |meta-security revision|

    URL: https://github.com/kraj/meta-clang
    layers: meta-clang
    branch: |meta-clang branch|
    revision: |meta-clang revision|

Additional layers are conditionally required, depending on the specific Cassini
distribution image being built and the platform being targeted. 

  .. code-block:: yaml
    :substitutions:

    URL: https://git.yoctoproject.org/git/meta-arm
    layers: meta-arm, meta-arm-bsp, meta-arm-toolchain
    branch: |meta-arm branch|
    revision: |meta-arm revision|

    URL: https://github.com/Wind-River/meta-secure-core
    layers: meta, meta-efi-secure-boot, meta-signing-key
    branch: |meta-secure-core branch|
    revision: |meta-secure-core revision|

    URL: https://gitlab.com/Linaro/trustedsubstrate/meta-ts.git
    layers: meta-trustedsubstrate
    branch: |meta-ts branch|
    revision: |meta-ts revision|

    URL: https://gitlab.com/Linaro/trustedsubstrate/meta-ledge-secure.git
    layers: meta-ledge-secure
    branch: |meta-ledge-secure branch|
    revision: |meta-ledge-secure revision|

    URL: https://github.com/Xilinx/meta-xilinx
    layers: meta-xilinx-core
    branch: |meta-xilinx branch|
    revision: |meta-xilinx revision|

  * Cassini SDK distribution images require ``meta-arm`` and
    ``meta-arm-toolchain``, as the ``gator-daemon`` package is installed on the
    rootfs.

  * Cassini firmware images built for the Corstone-1000 or N1SDP target
    platforms requires ``meta-arm``, ``meta-arm-bsp``, and
    ``meta-arm-toolchain``.

  * Cassini firmware images built for the Corstone-1000 also use
    ``efi-secure-boot`` and its dependencies to generate certificates for UEFI
    capsule images

  * Cassini firmware images built for KV260 also require
    ``meta-arm``, ``meta-arm-toolchain``, ``meta-trustedsubstrate``,
    ``meta-xilinx-core``, and ``meta-ledge-secure``.
