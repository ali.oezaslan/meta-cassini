..
 # SPDX-FileCopyrightText: Copyright (c) 2023, Linaro Limited.
 #
 # SPDX-License-Identifier: MIT

############
Introduction
############

Project Cassini defines a GitLab CI/CD pipeline to help developers and reviewers
by detecting issues at an early stage when a merge request is created.

To create a merge request on this project the user needs to be a member of the
project and refer to :doc:`../contributing` for further details.

********
Overview
********

The following diagram illustrates brief overview of Project Cassini automated
testing.

|

.. image:: ../images/gitlab_overview.svg
  :align: center

|

* **External Tools**:

  These tools are used by jobs in the pipeline and built in
  Docker containers ensuring running them in confined environment.

  * **Packages**:

    These are Debian packages provided from ubuntu.com

  * **jfrog**:

    The jfrog tool is provided from jfrog.io to communicate with :ref:`artifactory <artifactory_label>`.

  * **SCT parser**:

    The |SCT parser|_ is used to parse test results.

* **External Images**:

  External images used directly by the pipeline are pulled from the following
  sources:

  * **Base Image**:

    The Docker Hub is a service provided by Docker for finding and sharing
    container images. The images are some of the most secure images on Docker
    Hub and official released versions which can be used as a base image
    for usage.

  * **gitlab.com**:

    These are prebuilt images provided by gitlab.com to include jobs like
    static analysis.

  * **Quay**:

    A buildah image is provided by Red Hat (via Quay) to build custom
    container images. These images, which are used in later stages of
    the pipeline, are stored in the GitLab Registry.


* **GitLab CI/CD**:

  CI/CD is a GitLab feature which validates, request infrastructure for,
  and runs a GitLab pipeline using Container Registry.

  * **GitLab pipelines**:

    See |Gitlab pipelines|_

  * **Container Registry**:

    This stores custom container images which are used by some pipeline jobs.


************
Architecture
************

The following diagram illustrates various components involved when the user
raises a merge request for changes to be accepted for Project Cassini.

|

.. image:: ../images/gitlab_architecture.svg
  :align: center

|

The different sections are described below:

.. _gitlab_ci_cd_label:

* **Pipeline configuration**:

  This consists of further components:

  * **Dangerfile**:

    Configures and includes a danger-review job that is used to perform a
    variety of automated checks on the code under test.

    Danger is a Ruby Gem that runs in the CI environment.
    It only posts one comment and updates its content on subsequent
    danger-review runs.

  * **.gitlab-ci.yml**:

    The meta-cassini repository defines the configuration of pipeline, when
    user raises merge request for changes to be accepted.

  * **Gitlab Templates**:

    These provide templates when included for the project to enable/configure
    tools and functionalities for the pipelines which can be referred
    in :doc:`gitlab_templates`.

  * **.codeclimate.yml**:

    Configures Code Climate Plugin which is a Docker image that invokes a
    program that parses a config file and analyzes files, potentially
    producing formatted output representing issues in those files.

    Please refer related documentation in :doc:`code_quality` .

* **GitLab CI/CD**:

  A brief overview can be found in :doc:`gitlab_pipeline`.

  This consists of further components:

  * **GitLab Runners**:

    GitLab Runner is an application that works with :ref:`GitLab CI/CD <gitlab_ci_cd_label>`
    to run jobs in a pipeline.

  * **Container Registry**:

    The Container Registry holds the custom images required for specific
    pipeline jobs, which includes:

      * DangerBot Image
      * Code Climate plugins
      * FVP Images
      * LAVA Test Images
      * Utility Images

* **Testing**:

  This consists of further components:

  * **LAVA test framework**:

    The |LAVA|_ provides the capabilities to test the built images for Cassini
    distribution on supported target platforms.

.. _artifactory_label:

  * **Artifactory**:

    This provides a database to store results from the LAVA test framework.
