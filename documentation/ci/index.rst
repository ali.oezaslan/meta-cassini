..
 # SPDX-FileCopyrightText: Copyright (c) 2023, Linaro Limited.
 #
 # SPDX-License-Identifier: MIT

###############################################
Continuous  integration and development (CI/CD)
###############################################

.. toctree::
   :maxdepth: 2

   overview
   gitlab_templates
   code_quality
   gitlab_pipeline
