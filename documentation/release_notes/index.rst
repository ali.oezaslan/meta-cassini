..
 # SPDX-FileCopyrightText: Copyright (c) 2023, Linaro Limited.
 #
 # SPDX-FileCopyrightText: <text>Copyright 2022 Arm Limited and/or its
 # affiliates <open-source-office@arm.com></text>
 #
 # SPDX-License-Identifier: MIT

#############
Release Notes
#############

.. toctree::
   :maxdepth: 2

   v1.0.0
   v1.0.1
   v1.1.0
