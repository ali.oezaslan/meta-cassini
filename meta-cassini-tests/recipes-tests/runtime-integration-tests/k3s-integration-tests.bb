# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

SUMMARY = "K3s container orchestration integration tests."
DESCRIPTION = "Integration tests for the K3s container orchestration system. \
               Tests may be run standalone via \
               run-k3s-integration-tests, or via the ptest \
               framework using ptest-runner."
HOMEPAGE = "https://cassini.readthedocs.io/en/latest/"

LICENSE = "MIT"
# License file is in "layers/poky/meta/files/common-licenses".
# nooelint: oelint.var.licenseremotefile
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

TEST_FILES = "file://k3s-integration-tests.bats \
              file://k3s-funcs.sh \
              file://k3s-test-deployment.yaml"

inherit runtime-integration-tests
require runtime-integration-tests.inc

K3S_TEST_TYPE = "local deployment"
export K3S_TEST_TYPE

ENVSUBST_VARS:append = " \$K3S_TEST_TYPE"

do_install[vardeps] += "\
    K3S_TEST_TYPE \
    "
