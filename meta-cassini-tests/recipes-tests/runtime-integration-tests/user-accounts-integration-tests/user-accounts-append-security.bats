#!/usr/bin/env bats
#
# Copyright (c) 2022 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT
#
# Additional tests to be added to the user-accounts test suite, if running on
# a security-hardened image

export ROOT_USER="root"

load "${TEST_DIR}/user-accounts-security-funcs.sh"

@test 'user accounts management additional security tests' {

    subtest="Set password for '${NORMAL_USER}' account."
    _run check_user_local_access "${NORMAL_USER}"
    if [ "${status}" -ne 0 ]; then
        log "FAIL" "${subtest}"
        return 1
    else
        log "PASS" "${subtest}"
    fi

    subtest="Check umask setting for '${TEST_SUDO_USER}' account."
    _run check_umask
    if [ "${status}" -ne 0 ]; then
        log "FAIL" "${subtest}"
        return 1
    else
        log "PASS" "${subtest}"
    fi

    log "PASS"
}
