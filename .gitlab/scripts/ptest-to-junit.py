# Copyright (c) 2022 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT

# Converts ptest results to junit format

import argparse
import re

from junit_xml import TestSuite, TestCase, to_xml_report_file
from pathlib import Path


def _parse_test_logs(logs_folder, export_file):
    test_suites = []
    logs_path = Path(logs_folder)

    for test_log in logs_path.iterdir():
        if "stderr" not in test_log.stem:
            test_suites.append(
                _parse_test_log(test_log)
            )

    with open(export_file, 'w') as xml_file:
        to_xml_report_file(xml_file, test_suites)


def _parse_test_log(test_log):
    test_case = None
    test_cases = []
    collect_failure_log = False
    failure_log = ""
    with open(test_log, "r") as log_file:
        test_number = 0
        for line in log_file.readlines():
            if collect_failure_log:
                failure_log += line
                continue
            if "DEBUG" in line or "OUTPUT" in line:
                continue
            regex = r'(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}) ([A-Z]{4}):(.*)'
            test_result = re.match(regex, line)

            test_name = test_result.group(3).split(":")
            if test_result.group(2) == "FAIL":
                collect_failure_log = True

            if len(test_name) == 1:
                continue

            test_number += 1
            test_case = TestCase(test_name[1], test_number, "", "",
                                 timestamp=test_result.group(1))
            test_cases.append(test_case)

        if test_number == 0:
            test_case = TestCase(*test_name, test_number + 1, "", "",
                                 timestamp=test_result.group(1))
            test_cases.append(test_case)

        test_case.add_failure_info(output=failure_log)

    return TestSuite(test_log.stem, test_cases)


def main():
    parser = argparse.ArgumentParser(
                        description='Converts ptest results to junit format'
                        )

    parser.add_argument("logs_folder", help="ptest logs folder")
    parser.add_argument("output_file", help="Output Junit log file")

    args = parser.parse_args()

    _parse_test_logs(args.logs_folder, args.output_file)


if __name__ == '__main__':
    main()
