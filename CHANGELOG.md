## 1.1.0 (2023-12-17)

### other (30 changes)

- [cassini-config: Cassini release v1.1.0](Linaro/cassini/meta-cassini@707dfc3b54563cf2d24e09c6f1541d46d1165da3)
- [cassini-distro: Set distro version for release](Linaro/cassini/meta-cassini@9fb145ff022c08edd3aa9e2826203f1ea5df7495)
- [docs: Update Yocto layer documentation](Linaro/cassini/meta-cassini@4742d0a0a32ff7464866098b185f42cb8e364c7e)
- [cassini-config: Pin layers for Cassini v1.1.0](Linaro/cassini/meta-cassini@080046d694a6addf9b282014a737acd9a034b0ce)
- [cassini-[ci,docs]: Use newer PMIC firmware image for N1SDP](Linaro/cassini/meta-cassini@2a5f7d38056153618ea97134b713921798e838c3) ([merge request](Linaro/cassini/meta-cassini!156))
- [docs: Correct note about N1SDP hardware damage](Linaro/cassini/meta-cassini@2fdfb83d2a60f41f569336ad364c3f3be49c0b1e) ([merge request](Linaro/cassini/meta-cassini!156))
- [docs: Update kernel version for nanbield](Linaro/cassini/meta-cassini@77577bcfd0558f324153fa2124694fb24ea55378) ([merge request](Linaro/cassini/meta-cassini!152))
- [docs: Update references to kas to 4.0](Linaro/cassini/meta-cassini@123545cf605ec0c5ba0f461317af16d63c68718a) ([merge request](Linaro/cassini/meta-cassini!152))
- [docs: Update references to Yocto release for nanbield](Linaro/cassini/meta-cassini@75b66743cbabafe07453200ad5a011c92a4024ab) ([merge request](Linaro/cassini/meta-cassini!152))
- [ci: Configure release branch for nanbield](Linaro/cassini/meta-cassini@a158d212b4d347d3a6521b9a6dd4c79d507555af) ([merge request](Linaro/cassini/meta-cassini!152))
- [cassini-config: Track upstream nanbield](Linaro/cassini/meta-cassini@2e2767bf4d407ae4a5518f941ed689c14207a268) ([merge request](Linaro/cassini/meta-cassini!149))
- [docs: Update references to stable branch](Linaro/cassini/meta-cassini@6992bf9fa6b78a00461fabb70e133c14c07397d1) ([merge request](Linaro/cassini/meta-cassini!150))
- [ci: Ensure scheduled test jobs run at a low priority](Linaro/cassini/meta-cassini@c8fdb7f5c76f12e26a583c7a920540a80fc81fad) ([merge request](Linaro/cassini/meta-cassini!145))
- [cassini-[bsp,distro,tests]: Update LAYERSERIES_COMPAT for `nanbield`](Linaro/cassini/meta-cassini@d9d66b6a236b24d4bc1930ab70d95fad46212c51) ([merge request](Linaro/cassini/meta-cassini!117))
- [ci: Increase memory for cassini builds](Linaro/cassini/meta-cassini@13573f3a927efca8ff1a34669257bdf845a3b129) ([merge request](Linaro/cassini/meta-cassini!136))
- [ci: Update to Siemens kas 3.3 in CI](Linaro/cassini/meta-cassini@e19fc445e4e6be98cbc5f49b7670cb3271ed6b12) ([merge request](Linaro/cassini/meta-cassini!116))
- [cassini-distro: Use PACKAGECONFIG to select the Parsec config file](Linaro/cassini/meta-cassini@d05122e9f3ba38576cc0f677a36c1d1e785220a4) ([merge request](Linaro/cassini/meta-cassini!115))
- [cassini-distro: Use PARSEC_CONFIG to configure Parsec](Linaro/cassini/meta-cassini@78305ed73a33aa2384302b7dc196cd9df880e4d5) ([merge request](Linaro/cassini/meta-cassini!115))
- [meta-cassini-distro: Set a DISTROOVERRIDE for cassini features](Linaro/cassini/meta-cassini@7d5f497960afe7cf84eae37e267c3769dc671ba1) ([merge request](Linaro/cassini/meta-cassini!115))
- [[bsp, distro] Stop using c library to determine build type](Linaro/cassini/meta-cassini@93aab01c42fb92f198b2993a0837ccbfb6a58eff) ([merge request](Linaro/cassini/meta-cassini!115))
- [ci: Workaround for QPS error](Linaro/cassini/meta-cassini@6ea32b3fdb986f90faf528375690812ad03b2875) ([merge request](Linaro/cassini/meta-cassini!108))
- [ci: Enable oelint-adv checker](Linaro/cassini/meta-cassini@4b967085ff33dc32bc7b197a9ae26057a3b09813) ([merge request](Linaro/cassini/meta-cassini!108))

### bug (18 changes)

- [[bsp, distro] Move parsec configuration recipe](Linaro/cassini/meta-cassini@3b7fe9dac1c52115dad7593ab1460109629c5072) ([merge request](Linaro/cassini/meta-cassini!151))
- [ci: Ensure layer-checks are run on scheduled builds](Linaro/cassini/meta-cassini@21a8d0f5164a6287d2aca9e438566db8d8ef5c0c) ([merge request](Linaro/cassini/meta-cassini!142))
- [ci: New location for the Corstone-1000 FVP](Linaro/cassini/meta-cassini@972c179a08d4745691d58b3056e19c4003606277) ([merge request](Linaro/cassini/meta-cassini!142))
- [ci: Update GitLab template version](Linaro/cassini/meta-cassini@4cc639de7bd45fa9dba08d602769e8cb232ed98b) ([merge request](Linaro/cassini/meta-cassini!121))
- [cassini-bsp: Increase wait-online timeout for Corstone-1000](Linaro/cassini/meta-cassini@5393829bf8534f8269de719314f409fd17d72787) ([merge request](Linaro/cassini/meta-cassini!115))
- [[bsp, distro] Remove platform specific item from distro layer](Linaro/cassini/meta-cassini@bc6e1ca84518330105df071f2f772af12ccb6370) ([merge request](Linaro/cassini/meta-cassini!115))
- [ci: Update to new template version](Linaro/cassini/meta-cassini@b52b4955bd739d16d31af7062c8314e87dd33f14) ([merge request](Linaro/cassini/meta-cassini!108))
- [ci: Remove scheduled-or-manual rule from all images](Linaro/cassini/meta-cassini@2faf7769dc1f03ac8ffecbd853edcb9a42681e1a) ([merge request](Linaro/cassini/meta-cassini!101))
- [ci: Correct Corstone-1000 MPS3 image build](Linaro/cassini/meta-cassini@ad0cda4193d2a07e8fa8b940b91e08f9a64021b8) ([merge request](Linaro/cassini/meta-cassini!101))
- [ci: Ensure N1SDP ACS results are available](Linaro/cassini/meta-cassini@a8fcf4e20cc6d24ffadde188585a86ac0635b110) ([merge request](Linaro/cassini/meta-cassini!92))

### feature (4 changes)

- [ci: Adopt v2 of the Corstone-1000 FPGA image](Linaro/cassini/meta-cassini@eb2064682fdf0c598ac03632a146d9518cb48821) ([merge request](Linaro/cassini/meta-cassini!101))

## 1.0.1 (2023-10-31)

### other (3 changes)

- [cassini-[doc,config,distro]: Cassini 1.0.1 release](Linaro/cassini/meta-cassini@289ce064225d8da582e04dc301cb8674baeb8730)
- [cassini-config: Don't unpin upstream refspec for dev images](Linaro/cassini/meta-cassini@89bac3cb3245a0b925b28c8253325674dca2b79f)
- [ci: Disable headercheck (copyright/license)](Linaro/cassini/meta-cassini@fe0be77ebda4da988d09bdddecd8261fabd987bc)

### bug (1 change)

- [cassini-distro: Switch from `docker-ce` to `docker-moby`](Linaro/cassini/meta-cassini@ed0ce973ee4850ac4a63244c6fda9b9e9b36b15c)

### security (2 changes)

- [cassini-[config,doc]: Update meta-virtualization to fix docker](Linaro/cassini/meta-cassini@ed6633dfb04603d29a607cef9bc000afa8e428f6)
- [cassini-distro: Disable Parsec detailed error trace](Linaro/cassini/meta-cassini@9a34d83388a672e0e63e02075fd01e681d818103)

## 1.0.0 (2023-08-21)

### other (15 changes)

- [cassini-[doc,config,distro]: Cassini 1.0.0 release](Linaro/cassini/meta-cassini@917bc94fe71459d33cba2a3cbf8fa0206c0d6dcc)
- [docs: Add note that K3S is not supported on Corstone-1000](Linaro/cassini/meta-cassini@c0b502c4b38182ad5ad7f05c812ef510d302540f) ([merge request](Linaro/cassini/meta-cassini!69))
- [ci: Update to newer 1.5.1 pipeline template version](Linaro/cassini/meta-cassini@ad851b52c7855307c89f23da06dc4e6ed0144be5) ([merge request](Linaro/cassini/meta-cassini!46))
- [ci: Add output of jfrog upload log to test runs](Linaro/cassini/meta-cassini@819836cf48fd40e68d64fb9ceb24c6c2518a9640) ([merge request](Linaro/cassini/meta-cassini!46))
- [ci: Filter out test jobs if unable to run them under lava](Linaro/cassini/meta-cassini@02f5d30eba731723018c5a3105e8dceb2e0012d2) ([merge request](Linaro/cassini/meta-cassini!34))
- [ci: Switch to the cspell checker](Linaro/cassini/meta-cassini@a4cc4054f13998869810824583d9ef2cefaf5315) ([merge request](Linaro/cassini/meta-cassini!14))
- [doc: Move platform specific instructions to `Getting Started` guides](Linaro/cassini/meta-cassini@1a90f73092f2e3fb85d849a1d55a45efcb181fc8)
- [doc: Expand commit guidelines](Linaro/cassini/meta-cassini@4fe6634f59b81e1cf3463c5bc27a3681a6226311)
- [ci: Run Corstone-1000 FVP sanity test in lava farm](Linaro/cassini/meta-cassini@0f924cdfc165ae861f6c3219d85538fe246626cc)
- [[cassini-bsp, cassini-distro] Move platform specific settings](Linaro/cassini/meta-cassini@df47a58857634315074fb3cc9166c1d0616b375d)
- [cassini-distro: Explicitly set DISTRO_FEATURES](Linaro/cassini/meta-cassini@eadb77e2756aed02886a336ff197e3d0c0ebd626)
- [ci: Move to new template version v1.1.1](Linaro/cassini/meta-cassini@45922037eb71a3a072ec6848490cc0063391d42e)
- [ci: Add ACS test suite running](Linaro/cassini/meta-cassini@68f2ffb343db4970ab508f80ecbb12cb81aec4cc)
- [ci: Switch to using GitLab Code Quailty for qa_checks](Linaro/cassini/meta-cassini@039b439de6e8110f842593dedb226e6a53eb1ad9)
- [ci: Get lava results of test runs](Linaro/cassini/meta-cassini@0db0595e632678e7a995998855b39f19d28b1140)

### bug (15 changes)

- [cassini-bsp: Re-enable CGROUPS for docker (Corstone-1000)](Linaro/cassini/meta-cassini@e725985939fba2260b25170d58b69d84d486cbed) ([merge request](Linaro/cassini/meta-cassini!87))
- [cassini-bsp: Fix issues loading kernel modules on Corstone-1000](Linaro/cassini/meta-cassini@7893f800c366458ae58a5c216d44ca41e766f655) ([merge request](Linaro/cassini/meta-cassini!87))
- [meta-cassini-bsp: Always build the utilities image for corstone1000-mps3](Linaro/cassini/meta-cassini@ec27a74e1cdf7d257295dd0bd2be6673ec01fb13) ([merge request](Linaro/cassini/meta-cassini!46))
- [ci: Drop TLS use when uploading test results](Linaro/cassini/meta-cassini@c4899ebf48f03c3a66edbd00a220833e0b9d5784) ([merge request](Linaro/cassini/meta-cassini!46))
- [cassini-bsp: Reserve memory for SE comm on Corstone-1000](Linaro/cassini/meta-cassini@1a68841f3f1fe7be5816abe624608a407608f6e0) ([merge request](Linaro/cassini/meta-cassini!37))
- [ci: Modify lava tests scripts for Corstone-1000 MPS3](Linaro/cassini/meta-cassini@df46057d25496a07ccc6e633770099d13707e3a7) ([merge request](Linaro/cassini/meta-cassini!33))
- [distro: Fix cassini-dev distro feature builds](Linaro/cassini/meta-cassini@b775a7fe17eb60bd0b58ac242f27339a20077a6d)
- [bsp: Increase Corstone-1000 systemd device timeout](Linaro/cassini/meta-cassini@f2a25dd9b36fdc137c5a35f682a4898ecf433985)
- [bsp: Fix Corstone-1000 build configuration](Linaro/cassini/meta-cassini@8135bc7c4af753d24cef81293d5b5c7014971781)
- [Use v1.4.2 version of the templates](Linaro/cassini/meta-cassini@b722b38291064166e31a056157db874412c53fec)
- [ci: Use CI_REGISTRY_IMAGE in image paths](Linaro/cassini/meta-cassini@ae53147c5d8c8c472b1ef825d3c7ad597240f671)
- [ci: Workaround jfrog cli issue](Linaro/cassini/meta-cassini@7a0702a9b84eef18661b801bdd9097e61a1a6af0)
- [ci: Base kas image on latest-release version](Linaro/cassini/meta-cassini@3cc96e254fda1ea739a7a6a8ac3c66cb791a94f6)
- [[bsp,distro,test] Update layer compatibility](Linaro/cassini/meta-cassini@325da77bd0fb15ab201b67e5f7789cf7c2c6f805)
- [cassini-bsp/meta-arm: Add QSPI support to N1SDP edk2 build](Linaro/cassini/meta-cassini@f423c856416f4ce2c55a209739f0af9f9755772c)

### removed (1 change)

- [cassini-bsp: Remove K3S from Corstone-1000](Linaro/cassini/meta-cassini@907ca9491ea7f7fa042c0c405810e6f44b28d170) ([merge request](Linaro/cassini/meta-cassini!69))

### feature (7 changes)

- [cassini-bsp: Enable Block Storage Service on N1SDP](Linaro/cassini/meta-cassini@c9be323995f537d8c188ddecfaa5357d3cd7411e) ([merge request](Linaro/cassini/meta-cassini!21))
- [ci: Add acs testing to Corstone-1000 FVP](Linaro/cassini/meta-cassini@f4e1b9d2176e59d91372257191a6ccfff1ef3c22) ([merge request](Linaro/cassini/meta-cassini!19))
- [meta-cassini-config: Add kas menu configuration file](Linaro/cassini/meta-cassini@97bfb77ab0765b5426542a070b571da44e288d23) ([merge request](Linaro/cassini/meta-cassini!18))
- [[cassini-bsp, cassini-config] Add Corstone-1000 FVP to build](Linaro/cassini/meta-cassini@140620b5a1d2d70a75bb04c967e960e837bb1e95)
- [cassini-[docs,bsp,config,distro]: Add Corstone-1000-mps3 board support](Linaro/cassini/meta-cassini@fc039807910db227000d287a65604ea51f00f2cc)
- [meta-cassini-bsp/conf: Add support to build & install secure partitions](Linaro/cassini/meta-cassini@13fa88a3add002824b444ea23c70d75ef0696579)
- [cassini-[doc,config,distro,tests]: Add PARSEC support](Linaro/cassini/meta-cassini@e2cc34a7971b947f384379fa3d61244e42cb7751)

## 0.9.0 (2022-08-15)

No changes.

## 0.0.1 (2022-07-01)

No changes.
