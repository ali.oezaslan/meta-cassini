# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

PREFERRED_RPROVIDER_virtual-cloud-service ?= "no-cloud"

include ${@bb.utils.contains(\
'PREFERRED_RPROVIDER_virtual-cloud-service','k3s-cloud', \
'cloud-services/k3s.inc', '', d)}
