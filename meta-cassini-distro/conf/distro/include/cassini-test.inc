# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2022-2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# Config specific to the cassini-test distro feature, enabled using
# DISTRO_FEATURES

include ${@bb.utils.contains_any(\
'DISTRO_FEATURES','cassini-dev cassini-sdk', \
'', 'cassini-dev.inc', d)}

DISTRO_FEATURES:append:cassini = " ptest"

IMAGE_INSTALL:append:cassini = " jfrog-cli \
                                packagegroup-ts-tests-psa \
                                psa-arch-tests-ptest \
                                container-engine-integration-tests-ptest \
                                user-accounts-integration-tests-ptest \
                                ${@bb.utils.contains('DISTRO_FEATURES',\
                                'cassini-parsec', 'parsec-simple-e2e-tests-ptest', '', d)} \
                                "

TEECLNT_GROUP_ADD = "usermod -aG teeclnt ${CASSINI_TEST_ACCOUNT};"
EXTRA_USERS_PARAMS:prepend:cassini = "${@bb.utils.contains('MACHINE_FEATURES', 'ts-crypto ts-its', \
                                      '${TEECLNT_GROUP_ADD}', \
                                        bb.utils.contains('MACHINE_FEATURES', 'ts-se-proxy', \
                                        '${TEECLNT_GROUP_ADD}', '', d), d)}"

DISTROOVERRIDES .= ":cassini-test"
