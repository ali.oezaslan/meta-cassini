# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI:append:cassini = " file://config-ts.toml"

PACKAGECONFIG:cassini-parsec ?= "TS"

PARSEC_CONFIG:cassini-parsec ?= "${@bb.utils.contains('PACKAGECONFIG', 'TS', \
                          '${WORKDIR}/config-ts.toml', '${S}/config.toml', d)}"

