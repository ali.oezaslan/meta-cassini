# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2022-2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

IMAGE_BUILDINFO_VARS = "\
    BBMULTICONFIG DISTRO DISTRO_VERSION DISTRO_FEATURES IMAGE_FEATURES \
    IMAGE_NAME MACHINE MACHINE_FEATURES DEFAULTTUNE COMBINED_FEATURES "

inherit core-image extrausers image-buildinfo

IMAGE_FEATURES:cassini = "ssh-server-openssh bash-completion-pkgs"

# nooelint: oelint.vars.specific - Addition of FEATURE_PACKAGES
FEATURE_PACKAGES_cloud-service = "packagegroup-cloud-service"

IMAGE_INSTALL:append:cassini = " \
    bash \
    bash-completion-extra \
    ca-certificates \
    docker \
    kernel-modules \
    procps \
    sudo \
    wget \
    "

# Add two users: one with admin access and one without admin access
# 'CASSINI_USER_ACCOUNT', 'CASSINI_ADMIN_ACCOUNT'
EXTRA_USERS_PARAMS:prepend:cassini = "useradd -p '' ${CASSINI_USER_ACCOUNT}; \
                               useradd -p '' ${CASSINI_ADMIN_ACCOUNT}; \
                               groupadd ${CASSINI_ADMIN_GROUP}; \
                               usermod -aG ${CASSINI_ADMIN_GROUP} ${CASSINI_ADMIN_ACCOUNT}; \
                             "
