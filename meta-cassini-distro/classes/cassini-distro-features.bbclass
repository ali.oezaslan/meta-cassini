# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2022-2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# Used to apply to config for different features of the CASSINI distro, based on
# the contents of DISTRO_FEATURES

# Require inc file for development DISTRO_FEATURE
require ${@bb.utils.contains(\
'DISTRO_FEATURES','cassini-dev','conf/distro/include/cassini-dev.inc', '', d)}

# Require inc file for testing DISTRO_FEATURE
require ${@bb.utils.contains(\
'DISTRO_FEATURES','cassini-test','conf/distro/include/cassini-test.inc', '', d)}

# Require inc file for sdk DISTRO_FEATURE
require ${@bb.utils.contains(\
'DISTRO_FEATURES','cassini-sdk','conf/distro/include/cassini-sdk.inc', '', d)}

# Require inc file for security DISTRO_FEATURE
require ${@bb.utils.contains(\
'DISTRO_FEATURES','cassini-security',\
'conf/distro/include/cassini-security.inc', '', d)}

# Require inc file for PARSEC DISTRO_FEATURE
require ${@bb.utils.contains(\
'DISTRO_FEATURES','cassini-parsec',\
'conf/distro/include/cassini-parsec.inc', '', d)}

# Require inc file for Cloud DISTRO_FEATURE
require ${@bb.utils.contains(\
'DISTRO_FEATURES','cassini-cloud',\
'conf/distro/include/cassini-cloud.inc', '', d)}
