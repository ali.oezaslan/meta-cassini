# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

SUMMARY = "No Cloud Integration"
DESCRIPTION = "Dummy recipe when no cloud integration is required"
LICENSE = "MIT"
SRC_URI = ""

inherit packagegroup

RPROVIDES:${PN} += "virtual-cloud-service"
