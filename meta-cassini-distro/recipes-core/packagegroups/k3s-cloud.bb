# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

SUMMARY = "K3s Cloud Integration"
DESCRIPTION = "Include K3s related cloud integration"
LICENSE = "MIT"
SRC_URI = ""

inherit packagegroup

RDEPENDS:${PN} += "\
    ${@bb.utils.contains('DISTRO_FEATURES',\
                         'cassini-test', "${PN}-ptest", '', d)} \
    k3s-server \
"

RPROVIDES:${PN} += "virtual-cloud-service"
