# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

SUMMARY = "Cassini Cloud Service Integration"
DESCRIPTION = "Package recipe to include cloud service"
HOMEPAGE = "https://cassini.readthedocs.io/en/latest/"
LICENSE = "MIT"
SRC_URI = ""

inherit packagegroup features_check

REQUIRED_DISTRO_FEATURES = "cassini-parsec"

RDEPENDS:${PN} = "\
    virtual-cloud-service \
"
